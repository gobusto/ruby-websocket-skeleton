Simple Websocket Server
=======================

A minimal starting point for building websocket-based projects in Ruby.

Starting a server
-----------------

Make sure you have the `em-websocket` gem installed:

    gem install em-websocket

..then start a server:

    ruby server.rb

You can change the hostname and port using environment variables:

    HOST=127.0.0.1 PORT=1997 ruby server.rb

Otherwise, the host will default to `0.0.0.0` and the port will be `1337`.

How it works
------------

Aside from this README, there are only really two files you need to know about:

+ `server.rb` - contains the "glue" between the socket server and...
+ `app.rb` - this is the "main" file, where your actual code lives.

Don't worry too much about `server.rb` - just start adding code to `app.rb` and
expand your project from there.

There's also `example.html`, which demonstrates the basic code needed to get up
and running in a browser - this might be useful if you've never used websockets
before, and need some guidance in order to get started, but feel free to ignore
it if you already know what you're doing.

License
-------

Copyright (c) 2020 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
