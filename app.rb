# frozen_string_literal: true

# This is where everything begins...
class App
  def initialize
    # ...add any additional "setup" logic here...
    @connections = {}
  end

  def connect(cid, &socket)
    puts("New connection: #{cid}")
    @connections[cid] = { socket: socket, and_any: 'other details' }
  end

  def receive(cid, text)
    puts("Message from #{cid}: #{text}")
    send_to(cid, 'Pong!')
  end

  def disconnect(cid)
    puts("Closing connection: #{cid}")
    @connections.delete(cid)
  end

  private

  def send_to(cid, text)
    @connections[cid][:socket].call(text) if @connections[cid]
  end
end
